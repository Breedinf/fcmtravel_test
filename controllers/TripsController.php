<?php
/**
 * Created by PhpStorm.
 * User: Антон Матюшин
 * Date: 17.07.2018
 * Time: 22:30
 */

namespace app\controllers;

use app\models\FlightSegment;
use app\models\TripService;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\models\AirportName;
use app\models\Trip;

class TripsController extends Controller {

    public function actionIndexVariant1($search = "", $corporateId = 3, $serviceId = 2) {

        $start = microtime(true);
        $airportNames = AirportName::find()
            ->select('airport_id')
            ->distinct()
            ->where(['like', 'value', $search])->all();

        $airportIds = ArrayHelper::getColumn($airportNames, 'airport_id');

        $query = Trip::find()
            ->select('trip.*')
            ->distinct()
            ->joinWith('tripServices.flightSegments')
            ->where(['corporate_id', $corporateId])
            ->where(['trip_service.service_id', $serviceId])
            ->where(['in', 'flight_segment.depAirportId', $airportIds]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        return $this->render('trips', compact("dataProvider", "start"));
    }

    public function actionIndexVariant2($search = "", $corporateId = 3, $serviceId = 2) {

        $start = microtime(true);

        $airportSubQuery = AirportName::find()
            ->from('nemo.airport_name')
            ->select('airport_id')
            ->distinct()
            ->where(['like', 'value', $search]);

        $flightsSubQuery = FlightSegment::find()
            ->select('flight_id')
            ->distinct()
            ->where(['in', 'depAirportId', $airportSubQuery]);

        $serviceSubQuery =  TripService::find()
            ->select('trip_id')
            ->distinct()
            ->where(['service_id', $serviceId])
            ->where(['in', 'id', $flightsSubQuery]);

        $query = Trip::find()
            ->select('*')
            ->where(['corporate_id', $corporateId])
            ->where(['in', 'id', $serviceSubQuery]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);


        return $this->render('trips', compact("dataProvider", "start"));
    }

    public function actionIndexVariant3($search = "", $corporateId = 3, $serviceId = 2) {

        $start = microtime(true);

        $query = Trip::find()
            ->select('trip.*')
            ->distinct()
            ->joinWith('tripServices.flightSegments')
            ->where(['=', 'trip.corporate_id', $corporateId])
            ->where(['=', 'trip_service.service_id', $serviceId])
            ->joinWith(['tripServices.flightSegments.depAirportName' => function($q) use ($search) {
                return $q->from('nemo.' . AirportName::tableName())
                    ->where(['like', 'value', $search]);
            }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        return $this->render('trips', compact("dataProvider", "start"));
    }

}