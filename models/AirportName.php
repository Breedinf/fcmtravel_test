<?php
/**
 * Created by PhpStorm.
 * User: Антон Матюшин
 * Date: 17.07.2018
 * Time: 21:41
 */

namespace app\models;

use yii;
use yii\db\ActiveRecord;

class AirportName extends ActiveRecord {

    public static function tableName() {
        return '{{airport_name}}';
    }

    public static function getDb() {
        return Yii::$app->dbNemo;
    }


}