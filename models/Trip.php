<?php
/**
 * Created by PhpStorm.
 * User: Антон Матюшин
 * Date: 17.07.2018
 * Time: 21:43
 */

namespace app\models;


use yii\db\ActiveRecord;

class Trip extends ActiveRecord {

    public static function tableName() {
        return '{{trip}}';
    }

    public function getTripServices() {
        return $this->hasMany(TripService::className(), ['trip_id' => 'id'])->inverseOf('trip');
    }
}