<?php
/**
 * Created by PhpStorm.
 * User: Антон Матюшин
 * Date: 17.07.2018
 * Time: 21:43
 */

namespace app\models;


use yii\db\ActiveRecord;

class FlightSegment extends ActiveRecord {

    public static function tableName() {
        return '{{flight_segment}}';
    }

    public function getDepAirportName() {
        return $this->hasOne(AirportName::className(), ['airport_id' => 'depAirportId']);
    }

    public function getTripService() {
        return $this->hasOne(TripService::className(), ['flight_id' => 'id']);
    }

}