<?php
/**
 * Created by PhpStorm.
 * User: Антон Матюшин
 * Date: 17.07.2018
 * Time: 21:43
 */

namespace app\models;


use yii\db\ActiveRecord;

class TripService extends ActiveRecord {

    public static function tableName() {
        return '{{trip_service}}';
    }

    public function getTrip() {
        return $this->hasOne(Trip::className(), ['trip_id' => 'id']);
    }

    public function getFlightSegments() {
        return $this->hasMany(FlightSegment::className(), ['flight_id' => 'id'])->inverseOf('tripService');
    }

}