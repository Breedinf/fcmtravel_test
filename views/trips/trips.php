<?php

    use yii\grid\GridView;

    echo '<h3>It seems like variant2 is fastest... Execution time is below gridView</h3>';

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'user_id',
            'tag_le_id',
            'trip_purpose_id',
            'trip_purpose_desc',
            'status',
        ],
    ]);

    echo (microtime(true) - $start);


